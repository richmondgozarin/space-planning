from ferris import Model, ndb

class IdTracker(Model):
    unique_id = ndb.IntegerProperty(required=True, default=999)

    @staticmethod
    def generate_number(tracker_name,width=4):
        instance = ndb.Key(IdTracker, tracker_name).get()
        if not instance:
            instance = IdTracker(id=tracker_name)
        instance.unique_id += 1
        instance.put()
        return str(instance.unique_id).zfill(width)
