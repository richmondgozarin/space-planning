from ferris import Model, ndb


class Branch(Model):
    branch_code = ndb.StringProperty()
    branch_name = ndb.StringProperty()

    @classmethod
    def list_all(cls):
        return cls.query()

    @classmethod
    def create(cls, params):
        item = cls()
        item.populate(**params)
        item.put()
        return item
