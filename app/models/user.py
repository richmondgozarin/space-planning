from ferris import BasicModel, ndb


class User(BasicModel):

    role = ndb.StringProperty(choices=('admin', 'viewer',), default='viewer')
    branch = ndb.KeyProperty(kind='Branch')
    #name = ndb.StringProperty()
    email = ndb.StringProperty()

    @classmethod
    def list_all(cls):
        return cls.query()

    @classmethod
    def create(cls, params):
        item = cls()
        item.populate(**params)
        item.put()
        return item

    @classmethod
    def update_branch(cls, email, branch_key):
        params = {}
        instance = ndb.Key(cls, email.replace(' ', '_').lower()).get()
        if not instance:
            return None
        else:
            params['branch'] = branch_key

        item = cls()
        item.populate(**params)
        item.put()
        return item
