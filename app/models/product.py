from ferris import BasicModel, ndb


class Product(BasicModel):

    barcode = ndb.IntegerProperty()
    product_number = ndb.StringProperty()
    pecentage = ndb.FloatProperty()
    remarks = ndb.StringProperty(default="enabled")

    @classmethod
    def list_all(cls):
        return cls.query()

    @classmethod
    def create(cls, params):
        item = cls()
        item.populate(**params)
        item.put()
        return item
