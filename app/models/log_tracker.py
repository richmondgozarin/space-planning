from ferris import BasicModel, ndb


class LogTracker(BasicModel):
    email = ndb.StringProperty()
    details = ndb.StringProperty()

    @classmethod
    def list_all(cls):
        return cls.query()

    @classmethod
    def create(cls, params):
        item = cls()
        item.populate(**params)
        item.put()
        return item
