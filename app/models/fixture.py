from ferris import BasicModel, ndb


class Fixture(BasicModel):

    products = ndb.KeyProperty(kind='Product', repeated=True)
    barcode = ndb.IntegerProperty()
    fixture_number = ndb.StringProperty()
    dimension_x = ndb.FloatProperty(default=0.0)
    dimension_y = ndb.FloatProperty(default=0.0)
    category = ndb.StringProperty()
    remarks = ndb.StringProperty(default="enabled")

    @classmethod
    def list_all(cls):
        return cls.query()

    @classmethod
    def create(cls, params):
        item = cls()
        item.populate(**params)
        item.put()
        return item
