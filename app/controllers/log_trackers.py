from ferris import Controller, scaffold, route, route_with, messages
from app.models.log_tracker import LogTracker


class LogTrackers(Controller):
    class Meta:
        prefixes = ('api', 'admin')
        components = (scaffold.Scaffolding, messages.Messaging,)
        Model = LogTracker

    class Scaffold:
        display_properties = ('email', 'details','created', 'created_by')

    admin_add = scaffold.add
    admin_list = scaffold.list
    admin_delete = scaffold.delete
    admin_edit = scaffold.edit

    @route_with(template='/api/logTrackers', methods=['GET'])
    def api_list_all(self):
        self.context['data'] = LogTracker.list_all()

    @route
    def lists(self):
        pass
