from ferris import Controller, scaffold, route, route_with, messages
from app.models.user import User


class Users(Controller):
    class Meta:
        prefixes = ('api', 'admin')
        components = (scaffold.Scaffolding, messages.Messaging,)
        Model = User

    class Scaffold:
        display_properties = ('email','role', 'branch')

    admin_add = scaffold.add
    admin_list = scaffold.list
    admin_delete = scaffold.delete
    admin_edit = scaffold.edit

    @route_with(template='/api/users', methods=['GET'])
    def api_list_all(self):
        self.context['data'] = User.list_all()

    @route
    def lists(self):
        pass
