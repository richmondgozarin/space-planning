from ferris import Controller, scaffold, route, route_with, messages
from app.models.fixture import Fixture
from app.models.user import User
from app.models.branch import Branch
from google.appengine.api import users


class Fixtures(Controller):
    class Meta:
        prefixes = ('api', 'admin')
        components = (scaffold.Scaffolding, messages.Messaging,)
        Model = Fixture

    class Scaffold:
        display_properties = ('fixture_number', 'barcode', 'products', 'category', 'dimension_x', 'dimension_y', 'remarks')

    admin_add = scaffold.add
    admin_list = scaffold.list
    admin_delete = scaffold.delete
    admin_edit = scaffold.edit

    @route_with(template='/api/fixtures', methods=['GET'])
    def api_list_all(self):
        self.context['data'] = Fixture.list_all()

    @route
    def lists(self):
        pass

    @route
    def scan(self):
        user = users.get_current_user()
        email = user.email().lower()
        user_branch = {'selectedBranch': User.find_by_email(email).branch, 'branches': Branch.list_all()}
        self.context['branches'] = user_branch
