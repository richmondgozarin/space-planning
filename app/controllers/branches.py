from ferris import Controller, scaffold, route, route_with, messages
from app.models.branch import Branch


class Branches(Controller):
    class Meta:
        prefixes = ('api', 'admin')
        components = (scaffold.Scaffolding, messages.Messaging,)
        Model = Branch

    class Scaffold:
        display_properties = ('branch_code', 'branch_name')

    admin_add = scaffold.add
    admin_list = scaffold.list
    admin_delete = scaffold.delete
    admin_edit = scaffold.edit

    @route_with(template='/api/branches', methods=['GET'])
    def api_list_all(self):
        self.context['data'] = Branch.list_all()

    @route
    def lists(self):
        pass
