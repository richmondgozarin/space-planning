from ferris import Controller, scaffold, route, route_with, messages
from app.models.product import Product


class Products(Controller):
    class Meta:
        prefixes = ('api', 'admin')
        components = (scaffold.Scaffolding, messages.Messaging,)
        Model = Product

    class Scaffold:
        display_properties = ('product_number', 'product', 'barcode', 'percentage', 'remarks')

    admin_add = scaffold.add
    admin_list = scaffold.list
    admin_delete = scaffold.delete
    admin_edit = scaffold.edit

    @route_with(template='/api/products', methods=['GET'])
    def api_list_all(self):
        self.context['data'] = Product.list_all()

    @route
    def lists(self):
        pass
